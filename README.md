## Metagenomic Workflow

### Document Info
Creator ID: CNB</br>
Last Edit Date: 2020 AUG 12

## Programs and Dependencies
First, and most importantly, here is the list of programs that you will want to make sure that you have  on the server while you're working through this workflow. I have included, along with the program name, the program citations. Please use these and cite the people whose work will help you to analyze your metagenomes. 

* Screen
* Parallel
* Cutadapt

```
Martin, M. (2011). Cutadapt removes adapter sequences from high-throughput sequencing reads. EMBnet. journal, 17(1), 10-12.
```

* FastQC

```
Andrews, S. (2010). FastQC: a quality control tool for high throughput sequence data.
```

* Trim Galore

```
Krueger, F. (2015). Trim galore. A wrapper tool around Cutadapt and FastQC to consistently apply quality and adapter trimming to FastQ files, 516, 517.
```

* IDBA-UD; Remember to write down what version is downlaoded, as there is not an option that I can find to call up the version number.

```
Peng, Y., Leung, H. C., Yiu, S. M., & Chin, F. Y. (2012). IDBA-UD: a de novo assembler for single-cell and metagenomic sequencing data with highly uneven depth. Bioinformatics, 28(11), 1420-1428.
```

* SPAdes

```
Bankevich, A., Nurk, S., Antipov, D., Gurevich, A. A., Dvorkin, M., Kulikov, A. S., ... & Pyshkin, A. V. (2012). SPAdes: a new genome assembly algorithm and its applications to single-cell sequencing. Journal of computational biology, 19(5), 455-477.
```

* QUAST

```
Gurevich, A., Saveliev, V., Vyahhi, N., & Tesler, G. (2013). QUAST: quality assessment tool for genome assemblies. Bioinformatics, 29(8), 1072-1075.
```

* CMake

```
Martin, K., & Hoffman, B. (2010). Mastering CMake: a cross-platform build system. Kitware.
```

* [Sample Seqs](https://github.com/mooreryan/sample_seqs); Record accession date of the github page.

* MaxBin

```
Wu, Y. W., Simmons, B. A., & Singer, S. W. (2016). MaxBin 2.0: an automated binning algorithm to recover genomes from multiple metagenomic datasets. Bioinformatics, 32(4), 605-607.
```

* CONCOCT

```
Johannes Alneberg, Brynjar Smári Bjarnason, Ino de Bruijn, Melanie Schirmer, Joshua Quick, Umer Z Ijaz, Leo Lahti, Nicholas J Loman, Anders F Andersson & Christopher Quince. 2014. Binning metagenomic contigs by coverage and composition. Nature Methods, doi: 10.1038/nmeth.3103
```

* [pandas](https://github.com/pandas-dev/pandas)

```
McKinney, W., & Team, P. D. (2015). pandas: powerful Python data analysis toolkit. Pandas—Powerful Python Data Analysis Toolkit, 1625.
```

* MetaBAT

```
Kang, D. D., Froula, J., Egan, R., & Wang, Z. (2015). MetaBAT, an efficient tool for accurately reconstructing single genomes from complex microbial communities. PeerJ, 3, e1165.
```

* [bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)

``` 
Langmead B, Salzberg S. Fast gapped-read alignment with Bowtie 2. Nature Methods. 2012, 9:357-359.
```

* BinSanity

```
Graham, E. D., Heidelberg, J. F., & Tully, B. J. (2017). BinSanity: unsupervised clustering of environmental microbial assemblies using coverage and affinity propagation. PeerJ, 5, e3035.
```

* DAS Tool

```
Sieber, C. M., Probst, A. J., Sharrar, A., Thomas, B. C., Hess, M., Tringe, S. G., & Banfield, J. F. (2018). Recovery of genomes from metagenomes via a dereplication, aggregation and scoring strategy. Nature microbiology, 3(7), 836-843.
```

* CheckM

```
Parks, D. H., Imelfort, M., Skennerton, C. T., Hugenholtz, P., & Tyson, G. W. (2015). CheckM: assessing the quality of microbial genomes recovered from isolates, single cells, and metagenomes. Genome research, 25(7), 1043-1055.
```

* [pplacer](http://matsen.github.io/pplacer/compiling.html)

```
Matsen, F. A., Kodner, R. B., & Armbrust, E. V. (2010). pplacer: linear time maximum-likelihood and Bayesian phylogenetic placement of sequences onto a fixed reference tree. BMC bioinformatics, 11(1), 538.
```

##Trimming Your Reads

I started with fastq reads from Illumina NextSeq 3x150 from <http://cgeb-imr.ca/>. 
</br>
</br>
Reads usually come to you messy, so you'll need to trim off adapter sequences, etc. (Do this even if the people that sent you your sequences say they did.)
* Use the following code to move your sequences to the remote server you'll be using for your computations

```bash
scp /yourpc/seq.zip server@host
```
I don't recommend trying this on your personal laptop - it'll definitely crash somewhere around contig assembly. If you don't have access to a remote server then amazon does have a cloud computing option available.
</br>
</br>
Check that your machine has the program trim_galore, fastqc, and cutadapt. Remember to record all of the version numbers as you analyze your data. If you are on a shared server then it is possible that others will download a newer version later, so it's best to record this as you do it!

```bash
cutadapt --version

fastqc -v

~/yourfolder/TrimGalore-0.4.5/trim_galore -v
```
</br> 
The `trim_galore` program defaults to using phred quality scores (e.g. Illumina 1.9+). You will probably want to modify the minimum allowable phred score using the argumet `-q <INT>`.
</br>
</br>
Acceptable scores are typically within a range of 25-35. It is recommendable to try out multiple to see where your sequence loss and data quality converge. 
</br>
</br>
If you haven't already, create a "working" directory in your folder. Your server may have a different syntax for organizing where your folder and working folder should go. Verify with whomever maintains your server.

```bash
cd homefolder/your/path
mkdir homefolder/your/path/yourfolder
cd homefolder/your/path/yourfolder
mkdir working
cd working
```
</br>
Now you are inside your working directory - try to avoid working in the directory with the only copy of your files as it's super easy to accidentally delete them!!
</br>
</br>Now you'll want to start a "screen" so that, if your remote connection to your server is lost, your analysis will continue without you. This is especially important for any batched steps or longer commands. Otherwise, when you lose the connection, need to close your laptop, etc. then your work on the server will stop. 
</br>
</br>
First, check that screen is installed

```bash
screen --version
```
</br>
Have screen? Awesome! Now, create a screen that will be clearly labeled so you don't confuse your server mates! (e.g. your initials_program you're running)

```bash
screen -S session_name
```
</br>
To detach from screen use `ctrl+a, d`. To reconnect to your screen use the command `screen -r session_name`. Once you're finished with your screen _delete it_ by using the command `exit` from inside the screen window. 
</br>
</br>
Now, reconnect to your screen and from the directory with your sequence files run the following command:

```bash
parallel --plus -j 1 fastqc --threads 6 --paired -o your/file/path {} {/_R1_/_R2_} ::: your/file/path/*_R1_001.fastq.gz
```
</br>
Once you've run fastqc on your raw files, now you can run it for trimmed files and compare sequence loss for different `-q` scores. In the below code chunk I've opted for a few flags. First, note that we're running the fastqc program from within Trim Galore. Second, note that I have asked Trim Galore to `--retain_unpaired`. This will be important for later steps, as I will be comparing assemblies that save unpaired reads, as well as assembles that remove them. If you know already that you don't want to keep the unpaired reads then this flag isn't necessary. 

```bash
for FileName in *_R1_001.fastq.gz; do stem=`ls $FileName | cut -d"_" -f1,2,3`; your/path/TrimGalore-0.4.5/trim_galore --retain_unpaired --fastqc -q 30 --paired -o your/file/path $stem"_R1_001.fastq.gz" $stem"_R2_001.fastq.gz"; done
```
</br>
To compare the files it's easiest to checkout the .html file output, so I would use `scp` to move that to your local PC. 

```bash
scp youraccount@555.555.55.555: your/file/path/*fastqc.html your/desktop/
```
</br>
Remember that we're looking for somewhere in the range of 25-35 where your data quality and sequence loss converge on happy-medium. You'll want to run the Trim Galore program changing the `-q ##` flag within that range so that you can get an idea of what trimming quality works for your sequences. 25 is the least stringent, but may be necessary for certain sequences.
</br>
</br>
Now concatenate your files so that all the sequences go together :) It's best to do this by hand, unless you're sure you can code it to go in the right order. If your different reads get out of order then they won't assemble correctly downstream

```bash
cat BRO1_S12_L001_R2_001_val_2.fq BRO1_S12_L002_R2_001_val_2.fq BRO1_S12_L003_R2_001_val_2.fq BRO1_S12_L004_R2_001_val_2.fq > adayone_R2.fq
```
</br>
Now copy each of your cat files to a new folder. You'll want to save the original copy in case something goes wrong downstream, to save yourself the hassle of re-concatenating the files. My advice is to zip the old file path once you've moved the concatenated files to save room on the server. e.g., 

```bash
cp old/file/path/adayone_R2.fq new/file/path/adayone_R2.fq
```

##Assembly
Now, it's best to run the following on one or two of your sequences first as we'll be test-running a few different methods of assembly. First, I will introduce IDBA-UD. Please note that IDBA-UD hasn't been updated since approximately 2012. That, in itself, doesn't make it a bad program. It still does what it's supposed to do, but it is very lacking in documentation. Here I've pulled in what I was able to find as far as the way the program assembles (k-mers) and what commands you may want to use to optimize your assemblies. I will also be going over how to use SPAdes for assemblies. This program has better documentation and will provide you with more flexibility in your assemblies. Because both are still used in the literature it is worth exploring both, but ultimately the decision is between you and your PI.
</br>
</br>
As you can see from skimming below, there are a lot of options and each of these will take time to generate. Each SPAdes assembly took approximately 8 hours to complete. For this reason I recommend making a checklist (I used an excel document) with the program name, sample you are running, any modifications you have made using flags, any notes (I wrote down run times), and whether or not the run was completed. This way when you return the next day you can easily recall what your next step was (e.g. running a different method of assembly). For the following options I would recommend picking 2-3 of your samples and running those first through all the options to give you a general idea of how your sequences will best assemble. Then, complete the full assembly using one of them. 
</br>
</br>
If you already know how you'd like to assemble your sequences, feel free to skip to that part. 

###Assembly with IDBA-UD
To access the manual run the assembler without any commands. As you can probably tell, I'm a sloppy coder, so I haven't added the program to our default path - you can absolutely do that, or just call the program from folders like I do. 

```bash
your/file/path/idba/bin/idba_ud
```
</br> 
Next, we'll run the following command to interweave our forward and reverse (Here forward=R1, reverse=R2) .fq files and convert to .fa

```bash
your/file/path/idba/bin/fq2fa --merge --filter your/file/path/file_R1.fq your/file/path/file_R2.fq outputfilename.fa
```
</br>
Now it is possible to run the IDBA assembly for the .fa file!

```bash
your/file/path/idba/bin/idba_ud -num_threads 8 -l outputfilename.fa -o your/output/directory
```
</br>
I named my output directory idba100, that's because this initial assembly was for 100% of my sequences. Sometimes you get a better assembly if you subsample your reads. We'll go over subsampling in a bit. For now, note that you want to as descriptive as possible in as few characters as possible when naming your directories. This will help future you in figuring out where files are. Keeping good notes outside of the server of where you have put your output will also be important in the future, as you will want to go back through after you've completed a few steps and start zipping folders. This is important for minimizing the amount of space all of this (essentially) repeat information. It's also important to make sure that you have backed up your sequences and assemblies. Our server conveniently has a 5-disk RAID that automatically backs up information, but it is still best practice for us to move our sequences to a secondary location as well. (In the event all 5 disks go down at once.) 
</br>
</br>
For data back-up discuss with your PI what options you have in your lab. If they don't have a data back-up plan yet, then lucky you, you get to help make one!!
</br>
</br>
Now, back to our regularly scheduled programming. Next you'll want to check the quality of your assembly, we'll do this using `quast`
</br>
</br>From the directory with `contig.fa` (aka the output from the IDBA-UD assembly) run the following command. Note that I am using `metaquast`, this is the QUAST algorithm designed to handle metagenomic assemblies. Be sure that this is what you're using!

```bash
python your/path/quast-5.0.2/metaquast.py contig.fa
```
</br>
Don't forget to record the program version for QUAST. When you download it there should be a `.txt` file that includes the version number. 
</br>
</br>
You could also copy all of your contig files into one directory and run a for loop (like those we've run previously) on all of them at once. Either methodology is fine, I admit I'm just a chicken and didn't want to accidentally mislabel any of the original contig files. 

#### Batch Assembly with IDBA-UD
_**Don't forget to start your screen!**_
</br>
I couldn't figure out how to batch the fq2fa step. Sorry.

```bash
~/GitHub/idba/bin/fq2fa --merge --filter read_1.fq read_2.fq read.fa
```
</br>To peform a batch assembly set `-num_threads 8` so that it doesn't take _forever_. On our server we can allocate about 8 threads to a task without risking that the server will be overwhelmed. Be mindful of what you select as your thread number, as you'll want to leave threads open for accessory tasks. (Note that our server actually has 24 threads.) I also put in a `sleep` command as IDBA-UD doesn't run very smoothly. 
</br>
</br>
You can also use the flag `--seed_kmer` to change the kmer size, which is set to 30 by default in IDBA-UD. Here I haven't changed this, but you may want to play with the option depending on how your assemblies are going. 

```bash
for FileName in *fa; do /your/path/idba/bin/idba_ud -l $FileName -o /your/path/$FileName-assembly; sleep 20; done
```
</br>
**Quast Quality Scoring**

```bash
for FileName in *contig.fa; do python /your/path/quast/metaquast.py $FileName -o /your/path/$FileName-quast; done
```

###Assembly with SPAdes
Now let's try to assemble with SPAdes, because this program uses a different method of assembly it can have varying levels of success for different metagenomes. That said, both programs rely on k-mers for assembly. For this base assembly command all I'm calling are the `-1` forward and `-2` reverse reads to be used for generating the `metaspades` output. I do absolutely recommend using the `--tmp-dir` flag here, as it will put the temporary files in a directory separate from your ouput or input directories. This can be useful if the run stops in the middle, i.e. you can delete the temporary files. The flag `-t` tells SPAdes how many threads to use on your server.

```bash
your/path/SPAdes-3.13.0-Linux/bin/metaspades.py -1 /your/path/forwardread.fq -2 /your/path/reverseread.fq -o /your/path/outputdirectory -t 8 --tmp-dir /your/path/temp

python /your/path/quast-5.0.2/metaquast.py contigs.fasta
```
</br>
Here, I have made an output directory called "spades100" - we'll aslo be subsampling our SPAdes assembly. 
</br>
</br>
Also, obligatory reminder here to record the version of SPAdes that you are using. 

####Changing k-mers in SPAdes
As I mentioned with IDBA-UD, you can also change the k-mers you are using in SPAdes. To do this, I opted to "restart" my assemblies from the 'ec' step, or the error correction step. Essentially, right before the k-mers are assigned. You can also choose to restart from the `k55` step, for example, appending the k-mers 77, 99, and 127. To change the k-mers from default you can use the flag `-k`. The default k-mers for SPAdes are 21, 33, and 55. Note that you should not choose a k-mer value that is larger than the read length, so if you have very short reads it is not recommended that you use a long k-mer length. 
</br>
</br>
Note that, when using the `--restart-from` flag, your previous output will be overwritten. As this is the case, you will want to `cp` the files from the output folder to a new output folder!! 

```bash
/your/path/SPAdes-3.13.0-Linux/bin/metaspades.py --restart-from ec -k 21, 33, 55, 77,99,127 -o /your/path/output -t 8 --tmp-dir /your/path/temp
```

####Using unpaired reads in SPAdes
I also mentioned earlier, during the Trim Galore steps, that we would be retaining unpaired reads. Here's where we'll finally be using those! The biggest change here is that we are adding the flag `-s`, which retains the unpaired reads and includes them in our assembly. 

```bash
/your/path/SPAdes-3.13.0-Linux/bin/metaspades.py -1 /your/path/forwardreads.fq -2 /your/path/reversereads.fq -s /your/path/unpairedreads.fq -o /your/path/outputdir -t 8 --tmp-dir /your/path/temp
```
</br>
As a reminder, you'll want to use QUAST to quality score each of these assemblies once they've completed. All of your output will be `contigs.fa` from the SPAdes assembly, and will be in the various output folders you've assigned.

###Subsampling

To subsample we'll use the program `sample_seqs`. Below is an example of how to use this program. The flag `-p` allows you to choose a sampling percentage, in the below example I used 10%, and `-n` allows you to choose the number of times to sample I used 4. This will result in a subsample that is 40% of the original set of the trimmed reads. You can also include the unpaired reads using `-s`. No need to worry, the program will keep the forward and reverse reads together. 
</br>
Don't forget to record the version number you're using!

```bash
/your/path/sample_seqs_source/sample_seqs-0.1.5/build/sample_seqs -p 10 -n 4 -o your/path/subsampleoutput -b basename -1 /yourpath/forwardreads.fq -2 /yourpath/reversereads.fq
```
</br>
Once you've subsampled, you can run the IDBA-UD or SPAdes assemblies as previously described. Once you've completed those assemblies you can again compare the quast output for all of your assemblies. As I mentioned previously you'll want to `scp` the quast output .html file to your desktop to look at the graphs that were generated. 
</br>
</br>
You'll also want to start making an excel spreadsheet (or some other way) for tracking data on the assemblies. The values that will come in handy later are `Number Total Contigs (>0bp)`, `Max Contig (bp)`, `N50 (bp)`, `Total Bases (>500 bp)`, `Number Total Bases (>500 bp)`, `Number Contigs >500 bp`, `Number Contigs >1000 bp`. Using these statistics you can now make a decision as to how you are going to assemble your data! 


##Binning
####MaxBin
This is one where you should definitely get familiar with the [readme.txt](https://downloads.jbei.org/data/microbial_communities/MaxBin/README.txt). You have to include the `-contig` comand and the `-out` command. You have an option between `-abund`, `-reads`, `-abund_list`, or `-reads_list`. **Don't** include all of those as it will count your metagenome **twice**.
</br>
</br>
Running `perl /home/geomicroecu/GitHub/MaxBin-2.2.6/run_MaxBin.pl` will open the options menu. You can also call `-max_iteration #` 50 is the default for the maximum number of iterations. Including `-min_contig_length` allows you to change the minimum contig length you'll allow. If you want to maximize your bins then you could set that to 500, aka the smallest allowable contig length. Again, you could definitely integrate MaxBin into your default path, but I'm lazy so I just call it from the directory it's in. :) 
</br>
Reminder to write down the program version number! 

```bash
perl /your/path/MaxBin-2.2.6/run_MaxBin.pl -contig /your/path/contig.fa -out /your/path/output -reads /your/path/mergedreads.fa -thread 8 -min_contig_length 500 
```
In the above example I used the merged reads I made using fq2fa in the IDBA-UD program. This works for paired reads, but what if we want to include our unpaired reads from earlier? If you ran your assembly with unpaired reads and are looking to include those (or simply chose to use SPAdes and don't want to have to make a merged file) then you can use the below example, which provides three different locations for reads to MaxBin. 

```bash
perl /yourpath/MaxBin-2.2.6/run_MaxBin.pl -thread 8 -contig /your/path/contigs.fa -reads /your/path/forwardreads.fq -reads2 /your/path/reversereads.fq -reads3 /your/path/unpairedreads.fq -out /your/path/output
```

####CONCOCT

Wait! You cry, we just binned our contigs! Well, you're right. But, similarly to our sequence read assemblies, different binning algorithms will give you different results. For our purposes we'll be binning using several programs, then concatenating all of the results using DAS Tool. (If you want to know more about how DAS Tool works I recommend checking the paper out, it has a great explanation.) 
</br>
</br>
CONCOCT requires bam files and input, so the first thing you will need to do is generate bam files. To do this we will be using Bowtie 2. I drew my knowledge from [this tutorial](https://github.com/HadrienG/tutorials/blob/master/docs/meta_assembly.md) for how to do this.
</br> 
Don't forget to write down the program version number!

```bash
bowtie2-build /your/path/contigs.fasta /your/path/output.contigs

bowtie2 --threads 8 -x output.contigs -1 /your/path/forwardreads.fq -2 /your/path/reversereads.fq -U /your/path/unpairedreads.fq | \
    samtools view -bS -o output_to_sort.bam
    
samtools sort output_to_sort.bam -o sorted_output.bam

samtools index sorted_output.bam -@ 8
```
From the code you may have guessed that after you generate a .bam file with `bowtie2` you still have to sort and index that file using `samtools`. You would be correct! Once you've done that, though, you're in business!!
</br>
</br>
CONCOCT, as one of the binning programs, is probably one of the big doozies. You will be running CONCOCT from within the concoct_env in miniconda3. To start the CONCOCT environment use the command `conda activate concoct_env` and to leave type `conda deactivate`. 
</br>
</br>
The first chunk of code cuts the contigs into smaller parts (cut_up_fasta.py). The flag `-c` allows you to decide what size chunks to cut the contigs into. This is an optional command. The flag `-o` is the overlap size. `--merge_last` provides the script with the directive to concatenate the final part to the last contig. This will provide both a fasta file and a bed file, where the bed file has the coordinates of where each chunk came from the original contig. 
</br>
</br>
The next step is then to generate a table with coverage depth per sample and subcontig (concoct_coverage_table.py). This assumes that the directory you call has sorted and indexed .bam files. 
</br>
</br>
Finally, in the third step, you will run concoct!

```bash
/your/path/CONCOCT/scripts/cut_up_fasta.py /your/path/contigs.fasta -c 10000 -o 0 --merge_last -b contigs.bed > contigs_10K.fa

/your/path/CONCOCT/scripts/concoct_coverage_table.py contigs.bed /your/path/sorted_output.bam > coverage_table.tsv

concoct --threads 8 --composition_file contigs_10K.fa --coverage_file coverage_table.tsv -b /your/path/output/
```
Next you merge the subcontig clustering into the original contig clustering and extract the bins as individual FASTA.
```bash
/your/path/CONCOCT/scripts/merge_cutup_clustering.py /your/path/clustering_gt1000.csv > /your/path/clustering_merged.csv

mkdir /your/path/output/

/your/path/CONCOCT/scripts/extract_fasta_bins.py /your/path/contigs.fasta /your/path/clustering_merged.csv --output_path /your/path/output/
```

####MetaBAT
I installed MetaBAT using miniconda3, if you are using the docker image then you will need to use a different bit of code. In MetaBAT you can set the minimum size of a contig for binning with `-m`. There are, of course, other flags that you may find useful. You can find them in the README.md [here](https://gitlab.com/robegan21/MetaBAT/-/blob/master/README.md). 
</br>
I feel like I'm forgetting something... is it the version number?

```bash
metabat2 -t 8 -m 1500 -i /your/path/contigs.fasta -o /your/path/output your/path/sorted_output.bam
```

####BinSanity
I know I _should_ use BinSanity, but I spent a lot of time learning how to use these other programs. So for today just accept that I will get to this someday. 

####DAS Tool
By now you have several output directories filled with bins from different programs. They probably have varying numbers of bins. Because there won't be perfect synergy between these different algorithms, you'll need another algorithm to put it all together!
</br>
</br>
The first step in [DAS Tool](https://github.com/cmks/DAS_Tool) is converting the genome bins that are in fasta format to a scaffolds-to-bin table using `Fasta_to_Scaffolds2Bin.sh`. The flag `-e` gives the option to modify the extension, the default is `fasta`. This is fine for MaxBin, but the extension for some of your other bins may be `fa`, be mindful of that!
</br>
Speaking of mindfulness, don't forget that version number!

```bash
/your/path/DAS_Tool/src/Fasta_to_Scaffolds2Bin.sh -i /your/path/maxbinoutput -e fasta > /your/path/maxbin.scaffolds2bin.tsv
```
Because [concoct is extra complicated](https://github.com/cmks/DAS_Tool/issues/16), you'll use the `*_clustering_gt1000.csv`, which is a comma-separated scaffolds2bin file and just make that tab-seperated so that DAS Tool can read it.  

```bash
perl -pe "s/,/\tconcoct./g;" /your/path/clustering_gt1000.csv > /your/path/concoct.scaffolds2bin.tsv
```
Once you generate the scaffolds2bin files you can use those as input for DAS Tool. `-l` allows you be give a comma separated list of the binning prediction names. `-c` is where you provide the contigs in fasta format and `-o` is the output. The default for `--write_bin_evals` is 1 and the default for `--write_bins` is 0. The `--search_engine` you opt to use depends on the capabilities of your machine as well as your preferences. Here I chose to use `blast`, as the default `usearch` requires a large amount of memory.

```bash
DAS_Tool -i /your/path/concoct.scaffolds2bin.tsv,/your/path/maxbin.scaffolds2bin.tsv,/your/path/metabat.scaffolds2bin.tsv -l concoct,maxbin,metabat -c /your/path/contigs.fasta -o /your/path/output/ --write_bins 1 --write_bin_evals 0 --threads 8 --search_engine blast
```
Upon completion your output directory should have the consensus bins printed in fasta format!

####CheckM

For CheckM you need to make a folder with just the bin files. **Copy** all the files into the CheckM folder. Make sure that your original .fasta file didn’t get moved, too. To run CheckM you need to change any files ending in .fasta to .fa!

```bash
for f in *.fasta; do 
    mv -- "$f" "${f%.fasta}.fa"
done
```
We are so close to being done and you almost forgot the version number didn't you?

#####Lineage-Specific Work Flow
The Lineage workflow is recommended for assessing the completeness and contamination of genome bins. The output generated should inform the quality of your bins. The output will include `Completeness`, `Contamination`, and will include [other metrics](https://github.com/Ecogenomics/CheckM/wiki/Reported-Statistics).

```bash
checkm lineage_wf -t 8 -x fa /path/to/your/bins/ /your/path/output/
```

#####Taxonomy-Specific Work Flow 
The Taxonomy workflow is designed for analyzing the bins agains thte same marker set. In the below example I am using the phylum Proteobacteria. This is probably not useful for your metagenome, but I include it here for completeness. It may be useful in specifically looking at certain bins. For more information on [workflows](https://github.com/Ecogenomics/CheckM/wiki/Workflows) visit the CheckM github.

```bash
checkm taxonomy_wf -t 8 -x fa phylum Proteobacteria /path/to/your/bins/ /your/path/output/
``` 